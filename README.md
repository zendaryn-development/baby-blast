# Baby BLAST

**baby-blast.py** is a Python3 script that is designed to somewhat replicate
the functionality of the big-blast.pl tool from Biolinux.

```
usage: baby-blast.py [-h] [-f {fasta,genbank}] [-s SPLIT] [-p PROCESSES]
                     [-o OUTFILE] [-z] [-m {blastn,tblastx}]
                     blastdb query

positional arguments:
  blastdb               Path to the BLAST database to use
  query                 Path to the query to use

optional arguments:
  -h, --help            show this help message and exit
  -f {fasta,genbank}, --fmt {fasta,genbank}
                        Format of the query.
  -s SPLIT, --split SPLIT
                        Max number of nucleotides per split
  -p PROCESSES, --processes PROCESSES
                        Number of processes to use
  -o OUTFILE, --outfile OUTFILE
                        Outfile name
  -z, --force           Force overwrite of existing outfile
  -m {blastn,tblastx}, --mode {blastn,tblastx}
                        Mode to run comparison.

```


### Example usage
Assuming you have created a BLAST database, *x*, and have a query file, *y* in 
FASTA format, and wish to run a tblastx search using one process only, you can 
use the following command, which will create a file called out.crunch with
the results.

```
./baby-blast.py x y 
```

For more advanced use, you could consider running the command with all 
optional arguments specified.

```
./baby-blast.py -f fasta -s 50000 -p 8 -o x_vs_y.crunch -m tblastx x y 
```

Note, we use *./baby-blast.py* in these examples because we're assuming it is in
the same directory. However, you may place it on your PATH and call it using
*baby-blast.py* instead.

### Requirements
- A Python3 installation, with biopython installed.
- A NCBI BLAST+ installation that includes the tblastx and blastn binaries. 


### Acknowledgements
I would like to acknowledge Christian Atallah for his time spent testing this.