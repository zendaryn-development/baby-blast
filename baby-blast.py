#!/usr/bin/python3

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("blastdb", help="Path to the BLAST database to use")
parser.add_argument("query", help="Path to the query to use")
parser.add_argument("-f", "--fmt", help="Format of the query.", type=str, choices={"genbank", "fasta"}, default="fasta")
parser.add_argument("-s", "--split", help="Max number of nucleotides per split", type=int, default=50000)
parser.add_argument('-p', '--processes', help="Number of processes to use", type=int, default=1)
parser.add_argument('-o', '--outfile', help="Outfile name", type=str, default='out.crunch')
parser.add_argument('-z', '--force', help="Force overwrite of existing outfile", action='store_true')
parser.add_argument('-m', '--mode', help="Mode to run comparison.", type=str, choices={"tblastx", "blastn"}, default="tblastx")

args = parser.parse_args()

_QUERY = args.query
_BLASTDB = args.blastdb
_FMT = args.fmt
_SPLIT = args.split
_PROCESSES = args.processes
_OUTFILE = args.outfile
_FORCE = args.force
_MODE = args.mode

########################################

########################################

import os
import time
import subprocess
import multiprocessing
import warnings

warnings.simplefilter("ignore")


from Bio import SeqIO, SeqRecord, SearchIO


_LOCK = multiprocessing.Lock()

if os.path.isfile(_OUTFILE):
	if not _FORCE:
		raise Exception("Outfile already exists, please remove of use -z to force overwrite.")
	else:
		os.remove(_OUTFILE)

def fragment_fasta_file():
	fragments = []

	offsets = {}
	counter = 0

	for record in SeqIO.parse(_QUERY, _FMT):
		this = record.seq
		# Ensures that the last split isn't small.
		offset = 0
		while len(this) > _SPLIT * 2:
			# Get the split.
			split_seq = this[:_SPLIT]
			split_id = "{}".format(record.id)
			# Make a SeqRecord object and write to file.
			sro = SeqRecord.SeqRecord(id=split_id, seq=split_seq, description="")
			SeqIO.write(sro, '{}.fasta'.format(counter), 'fasta')
			fragments.append( '{}.fasta'.format(counter) )
			offsets['{}.fasta'.format(counter)] = offset * _SPLIT

			# Update this & counter.
			this = this[_SPLIT:]
			counter += 1 ; offset += 1

		# Process the last split.
		split_seq = this
		split_id = "{}".format(record.id)
		# Make a SeqRecord object and write to file.
		sro = SeqRecord.SeqRecord(id=split_id, seq=split_seq, description="")
		SeqIO.write(sro, '{}.fasta'.format(counter), 'fasta')
		fragments.append( '{}.fasta'.format(counter) )
		offsets['{}.fasta'.format(counter)] = offset * _SPLIT


	return fragments, offsets


def run_blast(infile, offsets, mode):
	if mode == "tblastx":
		command = [
			"tblastx",
			"-db",
			_BLASTDB,
			"-query",
			infile,
			"-outfmt",
			"6",
			"-out",
			"{}.out".format(infile),
			]

	elif mode == 'blastn':
		command = [
			"blastn",
			"-db",
			_BLASTDB,
			"-query",
			infile,
			"-outfmt",
			"6",
			"-out",
			"{}.out".format(infile),
			]

	else:
		raise Exception("Mode given not supported.")


	subprocess.call(command)

	_LOCK.acquire()

	crunch = open(_OUTFILE, 'a')

	for query in SearchIO.parse("{}.out".format(infile), 'blast-tab'):
		for hit in query:
			for hsp in hit:
				# Get relevant features to make a crunch file.
				score = hsp.bitscore
				identity = hsp.ident_pct

				if hsp.query_start > hsp.query_end:
					qstart = hsp.query_end + offsets[infile] ; qend = hsp.query_start + offsets[infile]
				else:
					qstart = hsp.query_start + offsets[infile] ; qend = hsp.query_end + offsets[infile]


				if hsp.hit_start > hsp.hit_end:
					hstart = hsp.hit_end ; hend = hsp.hit_start
				else:
					hstart = hsp.hit_start ; hend = hsp.hit_end


				crunch_details = [score, identity]
				crunch_details += [qend, qstart] if hsp.query_strand == -1 else [qstart, qend]
				crunch_details += [query.id]
				crunch_details += [hend, hstart] if hsp.hit_strand == -1 else [hstart, hend]
				crunch_details += [hit.id]

				crunch.write(
					"{0} {1} {2} {3} {4} {5} {6} {7}\n".format(*crunch_details))


	crunch.close()
	_LOCK.release()

	os.remove("{}.out".format(infile))
	os.remove(infile)

	print("Finished processing {}".format(infile))

def run():
	fragments, offsets = fragment_fasta_file()
	print(f"Query split into {len(fragments)} fragments")

	try:

		if _PROCESSES == 1:
			for fragment in fragments:
				run_blast(fragment, offsets,_MODE)

		else:

			P = multiprocessing.Pool(_PROCESSES)
			for fragment in fragments:
				P.apply_async(run_blast, (fragment, offsets,_MODE))
			P.close()
			P.join()

	except KeyboardInterrupt:
		print("Commensing soft exit: intermediate files.")
		for fragment in fragments:
			if os.path.isfile(fragment):
				os.remove(fragment)
			if os.path.isfile("{}.out".format(fragment)):
				os.remove("{}.out".format(fragment))

		if os.path.isfile(_OUTFILE):
			os.remove(_OUTFILE)

		raise KeyboardInterrupt

	print("Baby BLAST has finished running.")


if __name__ == '__main__':
	run()
